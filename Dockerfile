FROM alpine

RUN apk add bash jq openssl git curl aws-cli

RUN curl -o /usr/local/bin/kops https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64

CMD ["kops"]
